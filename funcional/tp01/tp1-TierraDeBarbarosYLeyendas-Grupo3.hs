import Text.Show.Functions
import Data.Char
type Nombre = String 
type Habilidad = String
type Fuerza = Int
type Pertenencia = Barbaro -> Barbaro
data Barbaro = Barbaro Nombre Fuerza [Habilidad] [Pertenencia] deriving Show

----------------------------
----- Algunos Bárbaros -----
----------------------------

judith = Barbaro "Judith" 100 ["tejer","escribirPoesia"] [ardilla , varitasDefectuosas "hacer magia"]

halion = Barbaro "Halion" 200 ["transformarse","escudarse","tocar la flauta"] [amuletosMisticos , varitasDefectuosas "volar", cuerda]

thrall = Barbaro "Thrall" 936 ["electrizar","incendiar"] [espadas , megafono, cuerda]

faffy = Barbaro "Faffy" 160 ["Cocinar","Escudar"] [megafono , varitasDefectuosas "Curar"]

gulldan = Barbaro "Gulldan" 2784 ["Hechizar"] [varitasDefectuosas "controlar demonios", ardilla, varitasDefectuosas "invocar infernales"]

lordtony = Barbaro "LordTony" 10 ["robar","desvanecerse"] [ardilla, amuletosMisticos]

varian = Barbaro "Varian" 670 ["Coordinar"] [ardilla, ardilla, espadas, megafono]

----------------------------------------
----- Funciones para extraer datos -----
----------------------------------------

habilidadesBarbaro _ _ _ habilidades _ = habilidades

nombreBarbaro _ nombre _ _ _ = nombre

fuerzaBarbaro _ _ fuerza _ _ = fuerza

pertenenciasBarbaro _ _ _ _ pertenencias = pertenencias

-------------------
----- Punto 1 -----
-------------------

espadas (Barbaro nombre fuerza habilidades pertenencias) = Barbaro nombre (fuerza + 2 * (length pertenencias)) habilidades pertenencias

amuletosMisticos (Barbaro nombre fuerza habilidades pertenencias) = Barbaro nombre fuerza (habilidades ++ ["clarividencia"]) pertenencias

varitasDefectuosas habilidad (Barbaro nombre fuerza habilidades pertenencias) = Barbaro nombre fuerza (habilidades ++ [habilidad]) [varitasDefectuosas habilidad]

cuerda (Barbaro nombre fuerza habilidades pertenencias) | (length pertenencias > 1)  = (head pertenencias) ((head (tail pertenencias)) (Barbaro nombre fuerza habilidades pertenencias))
                                                        | (length pertenencias == 1) = (head pertenencias) (Barbaro nombre fuerza habilidades pertenencias)

ardilla (Barbaro nombre fuerza habilidades pertenencias) = Barbaro nombre fuerza habilidades pertenencias

megafono (Barbaro nombre fuerza habilidades pertenencias) = Barbaro nombre fuerza [aMayus(unirHabilidades habilidades)] pertenencias
unirHabilidades habilidades | (length habilidades > 1) = (head habilidades) ++ (unirHabilidades (tail habilidades))
                            | (length habilidades == 1) = head habilidades
aMayus habilidades | (length habilidades > 1) = [(toUpper (head habilidades))] ++ (aMayus (tail habilidades))
                   | (length habilidades == 1) = [(toUpper (head habilidades))]

megafonoBarbarico (Barbaro nombre fuerza habilidades pertenencias) = megafono (cuerda (ardilla (Barbaro nombre fuerza habilidades pertenencias)))

-------------------
----- Punto 2 -----
-------------------

invasionDeDuendes (Barbaro nombre fuerza habilidades pertenencias) = elem "tocar la flauta" habilidades

cremalleraDelTiempo (Barbaro nombre fuerza habilidades pertenencias) = ("Faffy" == nombre) || ("Astro" == nombre)

ritualDeFechorias (Barbaro nombre fuerza habilidades pertenencias) = (saqueo fuerza habilidades) || (gritoDeGuerra habilidades pertenencias) || (caligrafia habilidades)

saqueo fuerza habilidades = (elem "robar" habilidades) && (fuerza > 80) 

gritoDeGuerra habilidades pertenencias = (length (unirHabilidades habilidades)) >= (4 * length pertenencias)

caligrafia habilidades | (length habilidades > 1) = (caligrafiaHabilidad (head habilidades)) && (caligrafia (tail habilidades))
                       | (length habilidades == 1) = caligrafiaHabilidad (head habilidades)
caligrafiaHabilidad habilidad = (('A' < head habilidad) && (head habilidad < 'Z')) && ((length (filter esVocal habilidad)) > 3)
esVocal letra = elem letra ("aeiou"++"AEIOU")

sobrevivientes :: [Barbaro] -> (Barbaro -> Bool) -> [Bool]
sobrevivientes (x:xs) aventura | length (x:xs) > 1 = [aventura x] ++ (sobrevivientes xs aventura)
                               | length (x:xs) == 1 = [aventura x]
sobrevivientes [] aventura = [False]

-------------------
----- Punto 3 -----
-------------------

sinRepetidos lista | lista == [] = []
                   | any (== head lista) (tail lista) = sinRepetidos (tail lista)
                   | otherwise = (take 1 lista) ++ sinRepetidos(tail lista)

descendientes x = infDescendientes x
infDescendientes (Barbaro nombre fuerza habilidades pertenencias) = [filtroHabilidades((aplicaPertenencias pertenencias) (Barbaro (nombre ++ "*") fuerza habilidades pertenencias))] ++ (infDescendientes (filtroHabilidades ((aplicaPertenencias pertenencias) (Barbaro (nombre ++ "*") fuerza habilidades pertenencias))))
aplicaPertenencias pertenencias | (length pertenencias > 1) = (head pertenencias).(aplicaPertenencias (tail pertenencias))
                                | (length pertenencias == 1) = (head pertenencias)
filtroHabilidades (Barbaro nombre fuerza habilidades pertenencias) = Barbaro nombre fuerza (sinRepetidos habilidades) pertenencias

-------------------
----- Punto 4 -----
-------------------

--  >:t sinRepetidos
--  sinRepetidos :: Eq t => [t] -> [t]
--  
--  La función se puede aplicar al nombre ya que un string es una lista de chars, que pertenecen a la clase Eq.
--  En el caso de las pertenencias es diferente, ya que a pesar de ser una lista está formada por funciones, y estas no pertenecen a la clase Eq.


