promediosSinAplazos notas = map (promedioNotas.aprobadas)notas

aprobadas lista = filter (>=4) lista

promedioNotas lista = (sum lista) `div` (length lista)

-- Para probarlo:
--
-- *Main> promediosSinAplazos [[10,4,10,2,6],[1,8,4,6]]
-- [7,6]
-- *Main> 
