esMultiploDeAlguno :: Int -> [Int] -> Bool
esMultiploDeAlguno _ [] = False
esMultiploDeAlguno num lista = any (esMultiplo num) lista
esMultiplo num otro = (mod) num otro == 0


esMultiploDeAlguno' :: Int -> [Int] -> Bool
esMultiploDeAlguno' _ [] = False
esMultiploDeAlguno' nro (cab:cola)	| (mod) nro cab == 0 = True
					| otherwise = esMultiploDeAlguno' nro cola
