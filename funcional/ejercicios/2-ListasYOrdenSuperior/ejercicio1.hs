primeras :: [[char]] -> Integer -> [[char]]

primeras [] _ = []
primeras _ 0 = []
primeras (cab:cola) n = cab : primeras cola (n-1)
