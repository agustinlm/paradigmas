type Empleado = String
type Gasto = Int

-- Usando foldl --
promedioGastos :: [(Empleado,Gasto)] -> Gasto
promedioGastos lista = (sumatoria lista) `div` (length lista) 

sumatoria lista = foldl suma 0 (lista)
suma sem (empleado,gasto) = sem + gasto

-- Usando foldr --
promedioGastos' :: [(Empleado,Gasto)] -> Gasto
promedioGastos' lista = (sumatoria' lista) `div`(length lista)

sumatoria' lista = foldr suma' 0 lista
suma' (empleado,gasto) sem = sem + gasto
