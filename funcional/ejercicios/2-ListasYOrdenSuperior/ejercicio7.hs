type Producto = String
type Precio = Int

-- productos - Definida con zip --
productos :: [Producto] -> [Precio] -> [(Producto,Precio)]
productos producto precio = zip producto precio

-- productos - Definida con zipWith --
productos' :: [Producto] -> [Precio] -> [(Producto,Precio)]
productos' producto precio = zipWith unir producto precio
unir a b = (a,b)
