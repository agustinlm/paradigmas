type Punto = (Int,Int)

-- Usando foldl --
cantidadPuntos :: [Punto] -> Int
cantidadPuntos puntos = foldl sumo 0 puntos

sumo sem _ = sem + 1

-- Usando foldr --
cantidadPuntos' :: [Punto] -> Int
cantidadPuntos' puntos = foldr sumo' 0 puntos

sumo' _ sem = sem + 1
