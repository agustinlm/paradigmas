data Alumno = Alumno Nombre Notas
type Nombre = String
type Notas = [Int]

uachines =[Alumno "pablo" [4,6,3,8,7,9,4], Alumno "Ezequiel" [5,4,5], Alumno "PepeLuis" [5,8]]


--Genera lista de Nombres APROBADOS--
aprobaron lista = map giveName (listaAprobados lista)

--Genera Lista de Alumnos APROBADOS--
listaAprobados alumnos = filter ((==True).snd.aprobo) alumnos

--Genera una tupla(Nombre,Condición)--
aprobo alumno = revision alumno 
revision (Alumno name notas)	| notas == [] = (name,False) 
				| (all (>=4) notas) = (name,True)
				| otherwise = (name,False)

--Devuelve el nombre del ALUMNO--
giveName (Alumno name _) = name
