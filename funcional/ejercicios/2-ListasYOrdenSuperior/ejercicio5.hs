data Alumno = Alumno Nombre Notas
type Nombre = String
type Notas = [Int]

alumno1 = Alumno "pablo" [4,6,6,8,7,9,4]

aprobo :: Alumno -> Bool
aprobo (Alumno _ notas) = revision notas
revision notas	| notas == [] = False
		| (all (>=4) notas) = True
		| otherwise = False
