type Nombre = String
type Notas = [Int]
data Persona = Alumno Nombre Notas

lista = [(Alumno "Juan" [4,6]),(Alumno "Clara" [3,5,8]),(Alumno "Christian" [3,4,5,6])]

promedioAlumnos :: [Persona] -> [(Nombre, Int)]
promedioAlumnos uachines = map promedio uachines

promedio :: Persona -> (Nombre, Int)
promedio (Alumno nombre notas) = (nombre, promediarNotas notas)

promediarNotas :: [Int] -> Int
promediarNotas notas = (sum notas) `div` (length notas) 
