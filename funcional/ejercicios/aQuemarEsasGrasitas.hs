import Text.Show.Functions
data Gimnasta = CGimnasta Nombre Edad Peso Tonificacion deriving(Show)
type Nombre = String
type Edad = Float
type Peso = Float
type Tonificacion = Float

data Rutina = Rutina NameRutina Duracion [Ejercicios]
type NameRutina = String
type Duracion = Float
type Dato = Float
type Ejercicios = (Dato -> Duracion -> Gimnasta -> Gimnasta)


-- Ejemplo de socios --
pancho = CGimnasta "Francisco" 40.0 120.0 1.0
andres = CGimnasta "Andy" 22.0 80.0 6.0
pepe = CGimnasta "Grillo" 16.0 40.0 1.0

-- El gimnata no hace nada --
relax :: a -> b -> b
relax min gimnasta = gimnasta
-------------------------------------------------------------------------------
-- 1 Indica si el Gimnasta es saludable --
saludable :: Gimnasta -> Bool
saludable (CGimnasta name _ peso tonif)	| (peso > 100.0 && tonif < 5.0) = False
					| otherwise = True

-- 2 Baja el peso del Gimnasta --
--quemarCalorias :: Gimnasta -> a -> Gimnasta
quemarCalorias (CGimnasta name edad peso t) cal	| peso > 100.0 = CGimnasta name edad (peso - (cal/150.0)) t
						| (peso < 100.0 && edad > 30.0 && cal > 200.0) = CGimnasta name edad (peso - 1.0) t
						| otherwise = CGimnasta name edad (peso - (cal/(peso*edad))) t

-- 3a1 Quema calorias caminando en cinta --
caminataEnCinta min gimnasta =  quemarCalorias gimnasta (1.0 * 5.0 * min)

-- 3a2 Quema calorias entrenando en cinta --
entrenamientoEnCinta min gimnasta = quemarCalorias gimnasta (1.0 * ((6+14)/2) * min)

-- 3b Tonifíca al gimnasta --
pesas weight min (CGimnasta n e p t)	| min <= 10 = CGimnasta n e p t
					| min > 10 = CGimnasta n e p (t + (weight/10))

-- 3c Quema calorias en colina --
colina inclan min gimnasta = quemarCalorias gimnasta (2 * min * inclan)

-- 3d Quema calorias en montaña --
montania inclan min gimnasta = colina (inclan+3) (min/2) (colina inclan (min/2) gimnasta)

-- 4ii Rutina de ejercicios --
laburaGimnasta (Rutina name time []) gimnasta = gimnasta
laburaGimnasta (Rutina name time (x:xs)) gimnasta = laburaGimnasta (Rutina name time xs) (x 20 time gimnasta)

ejercicios = [pesas, colina, montania]
rutina = Rutina "superArnold" 40 ejercicios
--------------------------------------------------------------------------------------------
-- GHCI --
-- *Main> laburaGimnasta (Rutina "granSiete" 40 [pesas,pesas,colina,montania,colina]) pancho 
-- CGimnasta "Francisco" 40.0 96.86667 5.0
-- *Main> laburaGimnasta (Rutina "granSiete" 40 [pesas,pesas,colina,montania,colina]) pepe 
-- CGimnasta "Grillo" 16.0 31.684692 5.0
--------------------------------------------------------------------------------------------

-- 4iii Rutina de ejercicios con fold ---


