-- Dada una lista de números conocer el máximo número usando fold --
--
-- \funcion:	maximo
-- \brief:	Recibe lista de numeros (restringidos), devuelve el número maximo.
-- \param1:	list - (restringida).
-- \comment:	Hay restricción del tipo Num a, Ord b.

maximo :: (Num b, Ord b) => [b] -> b
maximo [] = 0
maximo list = foldl max (head list) list

-- GHCI --
-- *Main> maximo [0,3,6,7,8,12,48,4]
-- 48
-- *Main> maximo [50,3,6,7,8,12,48,4]
-- 50

