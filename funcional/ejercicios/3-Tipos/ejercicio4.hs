-- \funcion:	h
-- \brief:	Recibe valor nom y una lista. Devuelve la cabeza de la lista
-- \param1:	nom - Valor de tipo a (restringido)
-- \param2:	lista - (Si no pongo lista en ambos lados, la funcion me la compila igual) (restringida)
-- \comment:	Hay restricción de tipo (Eq a)

h :: (Eq a) => a -> [(t1,a,t2)] -> (t1,a,t2)
h nom lista = (head.filter ((nom==).g)) lista
g (_, c, _) = c

-- GHCI --
-- *Main> h 5 [(2,3,1),(3,5,2),(4,8,1)]
-- (3,5,2)
-- *Main> 

