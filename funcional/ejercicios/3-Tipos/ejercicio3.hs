-- \funcion:	g
-- \brief:	Recibe funcion y dos valores del mismo tipo. Devuelve Bool.
-- \param1:	f - Funcion
-- \param2:	a - tipo (restringida)
-- \param3:	b - tipo (restringido)
-- \coment:	Hay restricción de tipo (Eq b)

g :: (Eq b) => (a -> b) -> a -> a -> Bool 
g f a b = f a == f b

-- GHCI --
-- *Main> g (*2) 4 5
-- False
-- *Main> g (*2) 5 5
-- True

-- *Main> g (++"a") "a" "s"
-- False
-- *Main> g (++"a") "a" "a"
-- True
