-- \funcion:	p
-- \brief:	Recibe funcion y lista. Devuelve la cabeza de la lista 
-- \param1:	n - Una funcion
-- \param2:	l - Una lista

lista = [1,3,8,2,4,5]

p :: (c -> Bool) -> [c] -> c
p n l = (head . filter n) l

-- GHCI --
-- *Main> p (>3) lista
-- 8
