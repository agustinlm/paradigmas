-- \funcion:	qfsort
-- \brief:	Ordena la ista ...
-- \param1:	f - Función 
-- \param2:	(x:xs) - Lista
-- \comment:	Hay restricción de Tipo (Ord b)

qfsort :: Ord b => (a -> b) -> [a] -> [a]
qfsort f [] = []
qfsort f (x:xs) = (qfsort f (filter ((> f x).f) xs)) ++ [x] ++ (qfsort f (filter ((< f x).f) xs))

