-- \funcion:	f
-- \brief:	Recibe funcion y dos valores del mismo tipo. Devuelve Bool.
-- \param1:	a - Valor tipo a(restringido)
-- \param2:	b - Función (restringida)
-- \param3:	(c:cs) - Lista de tipos a (restringido)
-- \coment:	Hay restricción de Tipo (Ord a)

f :: Ord a => a -> (a -> t) -> [a] -> t
f a b (c:cs)	| a > c = f a b cs
		| otherwise = b c

-- GHCI --
-- *Main> f 4 (*2) [1,2,3,4,5,6]
-- 8
-- *Main> f 4 (*2) [1,2,3,10,5,6]
-- 20
