-- \funcion:	f
-- \brief:	Recibe funcion y lista de tuplas. Devuelve Bool
-- \param1:	x - funcion (restringida)
-- \param2:	y - lista (restringida)
-- \coment:	Hay restricción de tipo (Ord c)

f :: Ord c => (a -> c) -> [(a,a)] -> Bool
f x y = (x.fst.head) y > (x.snd.head) y

-- GHCI --
-- *Main> f (*2) [(2,4),(3,4)]
-- False

