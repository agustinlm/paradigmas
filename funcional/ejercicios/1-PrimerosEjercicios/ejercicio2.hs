and' :: Bool -> Bool -> Bool
and' cond1 cond2	| cond1 = cond2
			| otherwise = cond1

and'' :: Bool -> Bool -> Bool
and'' True True = True
and'' _ _ = False
