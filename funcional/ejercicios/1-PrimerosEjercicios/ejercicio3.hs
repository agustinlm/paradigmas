type Nombre = String
type Notas = (Nota, Nota, Nota)
type Nota = Integer
data Persona = Alumno Nombre Notas 

christian = Alumno "Christian" (3,2,9)
agustin = Alumno "Agustin" (6,7,5)
clara = Alumno "Clara" (9,5,3)

notaMaxima :: Persona -> Nota
notaMaxima (Alumno nombre notas) = maxima notas

maxima::Notas->Nota
maxima (a,b,c) = a `max` b `max` c
