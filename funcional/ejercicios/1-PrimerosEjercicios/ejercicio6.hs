type Nombre = String
type Azucar = Integer
type Sabor = String
data Bebida = Cafe Nombre Azucar | Gaseosa Sabor Azucar

esEnergizante :: Bebida -> Bool
esEnergizante (Cafe tipo _)	| tipo == "capuchino" = True
				| otherwise = False
esEnergizante (Gaseosa sabor azucar)	| (sabor=="pomelo" && azucar > 10) = True
					| otherwise = False
