data Empleado = Comun Double String | Jefe Double Double String
type Plata = Double

christian = Comun 5000 "Christian"
agustin	= Jefe 10000 5 "Agustin"
clara	= Comun 6000 "Clara"


sueldo :: Empleado -> Plata
sueldo (Comun guita _) = guita
sueldo (Jefe guita cantEmpleados _) = guita * cantEmpleados
